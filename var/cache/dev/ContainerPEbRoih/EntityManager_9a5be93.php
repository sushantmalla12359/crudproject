<?php

namespace ContainerPEbRoih;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder279c3 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer38df0 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties18758 = [
        
    ];

    public function getConnection()
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'getConnection', array(), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'getMetadataFactory', array(), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'getExpressionBuilder', array(), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'beginTransaction', array(), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'getCache', array(), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->getCache();
    }

    public function transactional($func)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'transactional', array('func' => $func), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'wrapInTransaction', array('func' => $func), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'commit', array(), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->commit();
    }

    public function rollback()
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'rollback', array(), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'getClassMetadata', array('className' => $className), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'createQuery', array('dql' => $dql), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'createNamedQuery', array('name' => $name), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'createQueryBuilder', array(), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'flush', array('entity' => $entity), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'clear', array('entityName' => $entityName), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->clear($entityName);
    }

    public function close()
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'close', array(), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->close();
    }

    public function persist($entity)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'persist', array('entity' => $entity), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'remove', array('entity' => $entity), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'refresh', array('entity' => $entity), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'detach', array('entity' => $entity), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'merge', array('entity' => $entity), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'getRepository', array('entityName' => $entityName), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'contains', array('entity' => $entity), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'getEventManager', array(), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'getConfiguration', array(), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'isOpen', array(), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'getUnitOfWork', array(), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'getProxyFactory', array(), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'initializeObject', array('obj' => $obj), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'getFilters', array(), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'isFiltersStateClean', array(), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'hasFilters', array(), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return $this->valueHolder279c3->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer38df0 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder279c3) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder279c3 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder279c3->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, '__get', ['name' => $name], $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        if (isset(self::$publicProperties18758[$name])) {
            return $this->valueHolder279c3->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder279c3;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder279c3;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder279c3;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder279c3;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, '__isset', array('name' => $name), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder279c3;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder279c3;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, '__unset', array('name' => $name), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder279c3;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder279c3;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, '__clone', array(), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        $this->valueHolder279c3 = clone $this->valueHolder279c3;
    }

    public function __sleep()
    {
        $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, '__sleep', array(), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;

        return array('valueHolder279c3');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer38df0 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer38df0;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer38df0 && ($this->initializer38df0->__invoke($valueHolder279c3, $this, 'initializeProxy', array(), $this->initializer38df0) || 1) && $this->valueHolder279c3 = $valueHolder279c3;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder279c3;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder279c3;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
