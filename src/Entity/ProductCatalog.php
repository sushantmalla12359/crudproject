<?php

namespace App\Entity;

use App\Repository\ProductCatalogRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductCatalogRepository::class)
 */
class ProductCatalog
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;

    /**
     * @ORM\Column(type="integer")
     */
    private $Price;

    /**
     * @ORM\Column(type="date")
     */
    private $ManufactureDate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Certifications;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Constituents;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->Price;
    }

    public function setPrice(int $Price): self
    {
        $this->Price = $Price;

        return $this;
    }

    public function getManufactureDate(): ?\DateTimeInterface
    {
        return $this->ManufactureDate;
    }

    public function setManufactureDate(\DateTimeInterface $ManufactureDate): self
    {
        $this->ManufactureDate = $ManufactureDate;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getCertifications(): ?string
    {
        return $this->Certifications;
    }

    public function setCertifications(?string $Certifications): self
    {
        $this->Certifications = $Certifications;

        return $this;
    }

    public function getConstituents(): ?string
    {
        return $this->Constituents;
    }

    public function setConstituents(string $Constituents): self
    {
        $this->Constituents = $Constituents;

        return $this;
    }
}
