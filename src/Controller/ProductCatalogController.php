<?php

namespace App\Controller;

use App\Entity\ProductCatalog;
use App\Form\ProductCatalogType;
use App\Repository\ProductCatalogRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/product/catalog")
 */
class ProductCatalogController extends AbstractController
{
    /**
     * @Route("/", name="product_catalog_index", methods={"GET"})
     */
    public function index(ProductCatalogRepository $productCatalogRepository): Response
    {
        return $this->render('product_catalog/index.html.twig', [
            'product_catalogs' => $productCatalogRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="product_catalog_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $productCatalog = new ProductCatalog();
        $form = $this->createForm(ProductCatalogType::class, $productCatalog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($productCatalog);
            $entityManager->flush();

            return $this->redirectToRoute('product_catalog_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('product_catalog/new.html.twig', [
            'product_catalog' => $productCatalog,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="product_catalog_show", methods={"GET"})
     */
    public function show(ProductCatalog $productCatalog): Response
    {
        return $this->render('product_catalog/show.html.twig', [
            'product_catalog' => $productCatalog,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="product_catalog_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, ProductCatalog $productCatalog, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ProductCatalogType::class, $productCatalog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('product_catalog_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('product_catalog/edit.html.twig', [
            'product_catalog' => $productCatalog,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="product_catalog_delete", methods={"POST"})
     */
    public function delete(Request $request, ProductCatalog $productCatalog, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$productCatalog->getId(), $request->request->get('_token'))) {
            $entityManager->remove($productCatalog);
            $entityManager->flush();
        }

        return $this->redirectToRoute('product_catalog_index', [], Response::HTTP_SEE_OTHER);
    }
}
